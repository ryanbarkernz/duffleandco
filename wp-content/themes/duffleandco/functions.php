<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

function duffle_rego_menus() {
  register_nav_menu('footer-menu', __( 'Footer Menu' ));
}
add_action( 'init', 'duffle_rego_menus' );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */


function get_excerpt_char($count, $post_id = -1, $string = ''){
  if ($post_id != -1) $excerpt = apply_filters('the_content', get_post_field('post_content', $post_id));
  elseif ($string) $excerpt = $string;
  else $excerpt = get_the_content();

  $excerpt = trim( strip_tags($excerpt, '<sup>,<strong>,<em>') );
  if ( strlen($excerpt) > $count ) :
    $excerpt = substr($excerpt, 0, ($count-6)); // minus ' [...]'
    $excerpt = substr($excerpt, 0, strripos($excerpt, " ")) . ' [...]';
  endif;
  return $excerpt;
}

/**
 * Cart Link
 * Displayed a link to the cart including the number of items present and the cart total
 *
 * @return void
 * @since  1.0.0
 */
function storefront_cart_link() {
  $count = wp_kses_data( WC()->cart->get_cart_contents_count() );
  ?>
    <a class="view-cart-contents <?php echo $count == 0 ? 'view-cart--disabled' : ''; ?>" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'storefront' ); ?>">
      <span class="count"><?= $count; ?></span>
      <i class="icon-handbag icons"></i>
    </a>
  <?php
}


// Remove woocommerce select2 style and use our own
add_action( 'wp_enqueue_scripts', 'mgt_dequeue_stylesandscripts', 100 );
function mgt_dequeue_stylesandscripts() {
  if ( class_exists( 'woocommerce' ) ) {
    wp_dequeue_style( 'select2' );
    wp_deregister_style( 'select2' );

    wp_dequeue_script( 'select2');
    wp_deregister_script('select2');
  }
}


add_action('wp_enqueue_scripts', 'duffle_scripts');
function duffle_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

      wp_enqueue_script('jquery'); // Enqueue it!
        //wp_deregister_script('jquery'); // Deregister WordPress jQuery
        //wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', array(), '1.11.2');


        /**
         *
         * Minified and concatenated scripts
         *
         *     @vendors     plugins.min,js
         *     @custom      scripts.min.js
         *
         *     Order is important
         *
         */
        wp_register_script('duffle_vendorsJs', get_stylesheet_directory_uri() . '/assets/js/vendors.min.js', array('jquery'), '1.1', true); // Custom scripts
        wp_enqueue_script('duffle_vendorsJs'); // Enqueue it!

        wp_register_script('duffle_customJs', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array('jquery'),'1.1', true); // Custom scripts
        wp_enqueue_script('duffle_customJs'); // Enqueue it!

    }

}

/**
 *  Loads an alternate stylesheet, rather than the default style.css required by WordPress
 *  This does not replace the requirement of including a style.css in your theme
 *
 *  @author Ren Ventura <EngageWP.com>
 *  @link http://www.engagewp.com/load-minified-stylesheet-without-theme-header-wordpress
 *
 *  @param (string) $stylesheet_uri - Stylesheet URI for the current theme/child theme
 *  @param (string) $stylesheet_dir_uri - Stylesheet directory URI for the current theme/child theme
 *  @return (string) Path to alternate stylesheet
 */
add_filter( 'stylesheet_uri', 'rv_load_alternate_stylesheet', 10, 2 );
function rv_load_alternate_stylesheet( $stylesheet_uri, $stylesheet_dir_uri ) {
  // Make sure this URI path is correct for your file
  return get_stylesheet_directory_uri() . '/style.min.css';
}


function my_wpcf7_form_elements($html) {
    $html = str_replace('<option value="">---</option>', '<option value=""></option>', $html);
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');






// Colors!
function my_acf_input_admin_footer() {

?>
<script type="text/javascript">
(function($) {

  acf.add_filter('color_picker_args', function( args, $field ){
    args.palettes = ['#f7f7f7', '#ebebeb', '#e7e1d4', '#a8cfe7', '#f9a62c', '#ffd944', '#70cabf', '#394a81', '#161d33', '#234c53']
    return args;
  });

})(jQuery);
</script>
<?php

}


function my_mce4_options($init) {

    $custom_colours = '
        "f7f7f7", "Light Grey",
        "ebebeb", "Grey",
        "e7e1d4", "Light Brown",
        "a8cfe7", "Light Blue",
        "f9a62c", "Orange",
        "ffd944", "Yellow",
        "70cabf", "Downy",
        "394a81", "Blue",
        "161d33", "Primary Blue",
        "234c53", "Dark Green"
    ';

    $init['textcolor_map'] = '['.$custom_colours.']';
    $init['textcolor_rows'] = 2;
    return $init;
}

add_filter('tiny_mce_before_init', 'my_mce4_options');
add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');




// Overwrite Storefront page banner

function storefront_page_header() {
  $featured_image = get_the_post_thumbnail_url( get_the_ID(), 'large' );
  ?>
  <div class="hero">
    <div class="hero__image" style="background-image: url(<?= $featured_image ?>);"></div>
    <div class="container">
      <h1 class="large"><?= get_the_title(); ?></h1>
    </div>
  </div>
  <?php
}

function storefront_page_content() {
  ?>
  <section class="has-bg-color">
    <div class="container">
      <div class="entry-content">
        <?php the_content(); ?>
        <?php
          wp_link_pages( array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
            'after'  => '</div>',
          ) );
        ?>
      </div><!-- .entry-content -->
    </div>
  </section>
  <?php
}




if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Theme Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}


// Restrict users who can edit ACF fields
add_filter('acf/settings/show_admin', 'my_acf_show_admin');
function my_acf_show_admin($show) {
  // provide a list of usernames who can edit custom field definitions here
  $admins = array(
    'ryan'
  );

  // get the current user
  $current_user = wp_get_current_user();

  return (in_array($current_user->user_login, $admins));
}
