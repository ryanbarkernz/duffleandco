<?php
  global $post;
  global $post_slug;
  global $title;
  global $description;
  global $image;
  global $pageid;

  $pageid = get_the_ID();
  $post_slug = $post->post_name;

  if( is_search() ) {
    $frontpage_id = get_option( 'page_on_front' );
    $post = get_post( $frontpage_id );
    setup_postdata( $post );
  }

  $site_name = get_bloginfo('name');

  if( is_search() ) :
    $title = "Search results for '" . @$_GET["s"] . "' - " . get_bloginfo('name');
  elseif( get_post_meta(get_the_ID(), '_yoast_wpseo_title', true) ) :
    $title = get_post_meta(get_the_ID(), '_yoast_wpseo_title', true);
  elseif( is_home() && get_option('page_for_posts') ) :
    $title = strip_tags( get_page( get_option('page_for_posts') )->post_title ) . " - " . get_bloginfo('name');
  else :
    $title = strip_tags( get_the_title() ) . " - " . get_bloginfo('name');
  endif;

  if ( get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true) ) :
    $description = get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);
  elseif ( apply_filters('wp_trim_excerpt', get_post_field('post_excerpt', $pageid)) ) :
    $description = apply_filters('wp_trim_excerpt', get_post_field('post_excerpt', $pageid));
  else :
    $description = get_excerpt_char(155, $pageid);
  endif;

  if ( wp_get_attachment_url( get_post_thumbnail_id($pageid) ) ):
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($pageid), 'panel')[0];
  else :
    $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_on_front')), 'feature')[0];
  endif;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69839119-1', 'auto');
  ga('send', 'pageview');

</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='https://v2.zopim.com/?km3ZiH6xHok2RqvUXDyU14lFINtRJJii';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zendesk Chat Script-->

</head>

<body <?php body_class(); ?>
  data-cart-state="hidden"
  data-contact-state="hidden"
  data-handheld-nav="hidden"
  data-icon-guide-state="hidden"
  data-footer="default">

<div class="offcanvas-bg"></div>

<?php
if ( is_archive() ) get_template_part('inc/partials/_icon-guide');
?>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">

    <div class="header-top-wrap">
      <div class="container">
        <div class="header-top">
          <div class="header-top__column">
            <ul class="social">
              <li><a href="https://www.instagram.com/duffleandco" target="_blank" class="instagram"><i class="icon-social-instagram icons"></i></a></li>
              <li><a href="https://www.facebook.com/duffleandco" target="_blank" class="facebook"><i class="icon-social-facebook icons"></i></a></li>
            </ul>
          </div>
          <div class="header-top__column">
            <div class="announcement"><?php if (get_field('announcement', 'option')) : ?><i class="icon-direction icons"></i><?php the_field('announcement', 'option'); endif; ?></div>
          </div>
          <div class="header-top__column">
            <?php if ( has_nav_menu( 'secondary' ) ) : ?>
              <nav class="header-top__nav" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
                <?php
                  wp_nav_menu(
                    array(
                      'theme_location'  => 'secondary',
                      'fallback_cb'   => '',
                    )
                  );
                ?>
              </nav><!-- #site-navigation -->
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="header-bottom-wrap">
      <div class="container">

        <?php storefront_skip_links(); ?>

        <div class="header__left">
          <a href="/" class="site-logo"><img src="/ui/images/logo.svg" alt="Duffle and Co" aria-label="Duffle and Co"/></a>
          <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
            <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
            <?php
            wp_nav_menu(
              array(
                'theme_location'  => 'primary',
                'container_class' => 'primary-navigation',
                )
            );
            ?>
          </nav><!-- #site-navigation -->
        </div>

        <?php
          if ( storefront_is_woocommerce_activated() ) {
            if ( is_cart() ) {
              $class = 'current-menu-item';
            } else {
              $class = '';
            }
          ?>
          <ul class="header__cart">
            <li class="<?php echo esc_attr( $class ); ?>">
              <?php storefront_cart_link(); ?>
            </li>
          </ul>
          <?php
          }
        ?>

        <?php
  			/**
  			 * Functions hooked into storefront_header action
  			 *
  			 * @hooked storefront_skip_links                       - 0
  			 * @hooked storefront_social_icons                     - 10
  			 * @hooked storefront_site_branding                    - 20
  			 * @hooked storefront_secondary_navigation             - 30
  			 * @hooked storefront_product_search                   - 40
  			 * @hooked storefront_primary_navigation_wrapper       - 42
  			 * @hooked storefront_primary_navigation               - 50
  			 * @hooked storefront_header_cart                      - 60
  			 * @hooked storefront_primary_navigation_wrapper_close - 68
  			 */
  			//do_action( 'duffle_header' ); ?>

      </div>
      <div class="container">
      <?php
        wp_nav_menu(
          array(
            'theme_location'  => 'handheld',
            'container_class' => 'handheld-navigation',
            )
        );
        ?>
      </div>
		</div>
	</header><!-- #masthead -->

  <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>

  <div class="widget woocommerce contact-us text-white">
    <div class="contact-us__content">
      <header>
        <h4>Contact Us</h4>
        <a href="#" class="close-contact"><i class="icon-social-close icons"></i> Close</a>
      </header>

      <ul class="contact-us__information">
        <li><a href="mailto:info@duffleandco.com" class="icon-envelope icons">info@duffleandco.com</a></li>
        <li class="business-phone"><a href="tel:006448890725" class="icon-phone icons">+64 4 889 0725</a></li>
      </ul>

      <h2>Leave us a message</h2>
      <?php echo do_shortcode('[contact-form-7 id="56" title="Contact Us"]'); ?>

    </div>
  </div>

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">

		<?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		//do_action( 'storefront_content_top' );
