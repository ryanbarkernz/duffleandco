<?php
/**
 * Template name: Page Builder
 */

get_header();

$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'large' );
?>

  <main id="main" class="site-main" role="main">

    <div class="hero">
      <div class="hero__image" style="background-image: url(<?= $featured_image ?>);"></div>
      <div class="container">
        <h1 class="large"><?= get_the_title(); ?></h1>
      </div>
    </div>

    <?php
    get_template_part('inc/partials/_builder');
    ?>

	</main><!-- #main -->
<?php
get_footer();
