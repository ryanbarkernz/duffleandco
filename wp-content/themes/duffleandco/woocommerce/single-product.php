<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );

$id = get_the_ID();
?>

  <div class="product-nav">
    <div class="container">
      <div class="product-nav__column"><div class="product-nav__prev"><?php previous_post_link( '%link', '%title' ); ?></div></div>
      <div class="product-nav__column"><a href="/shop" class="product-nav__back">All Products</a></div>
      <div class="product-nav__column"><div class="product-nav__next"><?php next_post_link( '%link', '%title' ); ?></div></div>
    </div>
  </div>

  <section class="single-product product-hero">
    <div class="product-hero__bg"></div>
    <div class="container">
      <div class="grid">
        <div class="grid__column grid__column--6">
          <div class="product">
            <?php woocommerce_show_product_images(); ?>
          </div>
        </div>
        <div class="grid__column grid__column--6">
          <div class="summary entry-summary">
            <?php
              woocommerce_template_single_title();
              woocommerce_template_single_rating();
              woocommerce_template_single_price();
              w2mlaybuy_add_price_breakdown_for_text_and_table();
              w2mlaybuy_add_price_breakdown_for_text_only();

            // FEATURE ICONS
            $product_features = get_field('product_features') ? get_field('product_features') : [];
            $product_packaging_stationary = get_field('product_packaging_stationary') ? get_field('product_packaging_stationary') : [];
            $features = array_merge($product_features, $product_packaging_stationary);
            if( $features ): ?>
            <ul class="product__features">
              <?php foreach( $features as $feature ): ?>
                <li>
                  <i class="duffleicon duffleicon--<?= strtolower(str_replace(' ', '-', $feature)); ?>"></i>
                  <div class="tooltip"><?= $feature ?></div>
                </li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>

            <div class="product-hero__details">
              <h5>Details</h5>
              <?php
              global $post;
              if ($post->post_excerpt) echo wpautop($post->post_excerpt);
              ?>

            </div>

              <?php
                woocommerce_template_single_add_to_cart();
                // woocommerce_template_single_meta();
                // woocommerce_template_single_sharing();
                /**
                 * woocommerce_single_product_summary hook.
                 *
                WC_Structured_Data::generate_product_data();
                 */
                // do_action( 'woocommerce_single_product_summary' );
                do_action( 'woocommerce_before_single_product' );
              ?>

            </div><!-- .summary -->
        </div>
      </div>
    </div>
  </section>

  <?php get_template_part('inc/partials/_builder'); ?>

  <?php get_template_part('inc/partials/_related-products'); ?>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
