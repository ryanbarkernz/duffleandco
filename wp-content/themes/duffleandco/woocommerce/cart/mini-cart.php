<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_mini_cart' ); ?>

<header>
  <h4>Shopping Cart</h4>
  <a href="#" class="close-cart"><i class="icon-social-close icons"></i> Close</a>
</header>

<?php if ( ! WC()->cart->is_empty() ) : ?>

	<ul class="woocommerce-mini-cart cart_list <?php echo esc_attr( $args['list_class'] ); ?>">
		<?php
			do_action( 'woocommerce_before_mini_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
					$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
            <div class="cart-item__content">
  						<?php if ( ! $_product->is_visible() ) : ?>
  							<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name . '&nbsp;'; ?>
  						<?php else : ?>
                <a href="<?php echo esc_url( $product_permalink ); ?>" class="product__image" style="background-image: url(<?php echo wp_get_attachment_image_src($_product->image_id, 'medium')[0]; ?>);"></a>
                <div>
    							<a href="<?php echo esc_url( $product_permalink ); ?>">
                    <strong><?php echo $product_name; ?></strong> <?php echo $product_price ?>
                  </a>
                  <div class="quantity">Qty <?= $cart_item['quantity'] ?></div>

                  <?php
                  $features = array_merge(get_field('product_features', $product_id), get_field('product_packaging_stationary', $product_id));
                  if( $features ): ?>
                  <ul class="product__features">
                    <?php foreach( $features as $feature ): ?>
                      <li>
                        <i class="duffleicon duffleicon--<?= strtolower(str_replace(' ', '-', $feature)); ?>"></i>
                        <div class="tooltip"><?= $feature ?></div>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                  <?php endif; ?>
                </div>

              <?php endif; ?>
            </div>

            <?php
            echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
              '<a href="%s" class="remove-from-cart" aria-label="%s" data-product_id="%s" data-product_sku="%s">REMOVE</a>',
              esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
              __( 'Remove this item', 'woocommerce' ),
              esc_attr( $product_id ),
              esc_attr( $_product->get_sku() )
            ), $cart_item_key );
            ?>
					</li>
					<?php
				}
			}

			do_action( 'woocommerce_mini_cart_contents' );
		?>
	</ul>

  <div class="cart__total">
  	<h4 class="woocommerce-mini-cart__total"><strong><?php _e( 'Subtotal', 'woocommerce' ); ?></strong> <?php echo WC()->cart->get_cart_subtotal(); ?></h4>
    <small>Excludes Shipping</small>
  </div>

	<p class="woocommerce-mini-cart__buttons">
    <a href="<?= esc_url( wc_get_checkout_url() ) ?>" class="button-blue">Checkout</a>
  </p>

<?php else : ?>

	<p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
