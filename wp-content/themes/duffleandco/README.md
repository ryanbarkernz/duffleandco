# Duffle and Co Child Theme

The Duffle and Co Child Theme is a child theme for WooThemes StoreFront WooCommerce theme.

## Usage

This child theme is designed to be used as a child theme for the WooCommerce StoreFront theme which you can download for free below.

* [Download WooCommerce StoreFront Theme](https://wordpress.org/themes/storefront/)
* [StoreFront Documentation](http://docs.woocommerce.com/documentation/themes/storefront/)
* [StoreFront Child Themes](https://woocommerce.com/product-category/themes/storefront-child-theme-themes/)
* [StoreFront Extensions](https://woocommerce.com/product-category/storefront-extensions/)

Custom PHP that you write should be added to the child themes functions.php file whilst any custom CSS should be added to the child themes style.css file.

There is also a style.scss file within the /assets/sass/ folder that can be used if you wish to write [SASS - Syntactically Awesome Style Sheets](http://sass-lang.com/) based styles which can then be compiled into the style.css file using an app like [CodeKit](https://incident57.com/codekit/) for OSX or [PrePros](https://prepros.io/) for Windows.

If you would like to learn more about child themes for WordPress see this documentation below.

* [WordPress Child Themes](https://codex.wordpress.org/Child_Themes)

Read: [Introducing WPGulp: An Easy to Use WordPress Gulp Boilerplate](https://ahmadawais.com/introducing-wpgulp-an-easy-to-use-wordpress-gulp-boilerplate/)
