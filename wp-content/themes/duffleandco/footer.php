<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

	</div><!-- #content -->


	<footer class="footer">
		<div class="container">
      <a href="/" class="footer__logo"><img src="/ui/images/logo-white.svg" alt="Duffle and Co" width="80" aria-label="Duffle and Co"/></a>

      <?php echo do_shortcode('[contact-form-7 id="40" title="Footer Newsletter Signup"]') ?>

      <nav>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => false ) ); ?>
      </nav>

      <ul class="copyright">
        <li>© Copyright Duffle&amp;Co 2017</li>
        <li><a href="/terms-conditions">Terms &amp; Conditions</a></li>
        <?php if ( get_field('email', 'option') ) : ?><li>E <a href="mailto:<?= get_field('email', 'option'); ?>"><?= get_field('email', 'option'); ?></a></li><?php endif; ?>
        <li>Design by <a href="/" class="L">L</a></li>
      </ul>

      <ul class="social">
        <?php if ( get_field('instagram', 'option') ) : ?>
          <li><a href="<?= get_field('instagram', 'option'); ?>" target="_blank" class="instagram"><i class="icon-social-instagram icons"></i></a></li>
        <?php endif; ?>
        <?php if ( get_field('facebook', 'option') ) : ?>
          <li><a href="<?= get_field('facebook', 'option'); ?>" target="_blank" class="facebook"><i class="icon-social-facebook icons"></i></a></li>
        <?php endif; ?>
        <?php if ( get_field('twitter', 'option') ) : ?>
          <li><a href="<?= get_field('twitter', 'option'); ?>" target="_blank" class="twitter"><i class="icon-social-twitter icons"></i></a></li>
        <?php endif; ?>
        <?php if ( get_field('linkedin', 'option') ) : ?>
          <li><a href="<?= get_field('linkedin', 'option'); ?>" target="_blank" class="linkedin"><i class="icon-social-linkedin icons"></i></a></li>
        <?php endif; ?>
        <?php if ( get_field('youtube', 'option') ) : ?>
          <li><a href="<?= get_field('youtube', 'option'); ?>" target="_blank" class="youtube"><i class="icon-social-youtube icons"></i></a></li>
        <?php endif; ?>
      </ul>

		</div>
	</footer>


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
