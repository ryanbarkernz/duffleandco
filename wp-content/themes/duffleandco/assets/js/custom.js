/*

  extra Math functions

*/

(function ($) {

  $("body").on('click', "a[href^=#]", function (e) {
    e.preventDefault();

    var $el = $($(this).attr('href'));
    if(!$el.length) return;

    $('html,body').animate({scrollTop: $el.offset().top}, 400);
  });

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.utilityDemo = function( options ) {

    var settings = {
      setting: ''
    },

    fn = {
      func: function (e) {
        console.log('func')
      },

      func2: function (e) {
        console.log('func2')
      },
    };

    //
    // Make some events accessible from global scope
    //
    return {
      func: fn.func,
      func2: fn.func2
    };
  };


})(jQuery);

// usage: duffle.utilityDemo().func1()

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.accordion = function( options ) {

    var settings = {
      el: {
        accordion: ".panels",
        toggle: ".panel",
        content: ".panels__content > div",
      },
      speed: 300,
      active: false
    },

    fn = {
      init: function () {

        if(!$(settings.el.accordion)) return;
        $(settings.el.toggle).on('click', fn.toggle);

      },

      toggle: function (e) {

        var $el = $(e.currentTarget),
            id = '#' + $el.attr('data-id'),
            $next = $(id);

        // Toggle data state on the accordion toggle
        if ( $el.attr('data-state') === 'active' ) {
          $el.attr('data-state', '');
          $(settings.el.accordion).attr('data-state', 'closed');
        } else {
          $(settings.el.toggle).attr('data-state', '');
          $el.attr('data-state', 'active');
          $(settings.el.accordion).attr('data-state', 'open');
        }

        // Expand or collapse this accordion content
        $next.addClass('is-active').slideToggle(settings.speed, fn.refresh);

        if($next.find('.image-slider').length !== 0) $next.find('.image-slider').get(0).slick.setPosition();

        // Hide the other panels
        $(settings.el.content).not($next).removeClass('is-active').slideUp(settings.speed);

      },

      refresh: function () {
        Waypoint.refreshAll();
      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.featuredProducts = function( options ) {

    var settings = {
      el: {
        category: ".fp__categories > a",
        product: ".fp__products > li",
      },
      id: 'all',
    },

    fn = {
      init: function () {

        $(settings.el.category).on('click', fn.filter);
        settings.limited = $('.featured-products--limited').length > 0;

        settings.id = $(settings.el.category).length > 0 ? $(settings.el.category).first().attr('data-id') : 'all';

        fn.filter();

        $('.woocommerce-product-gallery').find('.flex-control-thumbs img').after('<div></div>');

      },

      filter: function (e) {

        if(e) {
          e.preventDefault();
          var $el = $(e.currentTarget);
          settings.id = $el.attr('data-id');
        }

        $(settings.el.category).removeClass('active');
        $('[data-id="' + settings.id + '"]').addClass('active');

        var i = 0;

        $(settings.el.product).each(function () {
          if($(this).hasClass('fp__link')) return $(this).attr('data-visibility', 'visible').attr('data-order', i);

          $(this).attr('data-visibility', 'hidden').attr('data-order', -1).removeClass('fp__product--offset');

          if (i < 3 || !settings.limited) {
            if (settings.id == 'all') {
              $(this).attr('data-visibility', 'visible').attr('data-order', i);
              i++;
            } else if ($(this).attr('data-cat').indexOf(settings.id) >= 0) {
              $(this).attr('data-visibility', 'visible').attr('data-order', i);
              i++;
            }
            if(i % 2 !== 0) $(this).addClass('fp__product--offset');
          }

        })

      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.footer = function( options ) {

    var settings = {
      el: {
      },
    },

    fn = {
      init: function () {
        $(window).on('resize', fn.fixFooter);
        fn.fixFooter();
      },

      fixFooter: function () {
        if ($('.footer .container').innerHeight() + 200 < window.innerHeight) {
          $('body').attr('data-footer', 'fixed');
        } else {
          $('body').attr('data-footer', 'default');
        }
      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.forms = function( options ) {

    var settings = {
      el: {
        selects: '#content select',
        contactSelect: '.contact-us .wpcf7 select',
        footerSelect: '.footer .wpcf7 select',
        email: '.wpcf7 input[type="email"]',
        variableEmailWrap: '.footer .wpcf7 .mc4wp-EMAIL',
        label: 'label.placeholder',
        numbers: 'label.placeholder',
      },
    },

    fn = {
      init: function () {

        $(settings.el.footerSelect).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: 'auto',
        }).data('select2').$dropdown.addClass('footer-select-dropdown');

        $(settings.el.selects).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: '100%',
        }).focus(function () { $(this).select2('focus'); });

        $(settings.el.contactSelect).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: '100%',
        }).focus(function () { $(this).select2('focus'); })
          .data('select2').$dropdown.addClass('contact-select-dropdown');

        // $(settings.el.email).on('keyup', fn.updateEmailLength)
        $(settings.el.variableEmailWrap)
          .append('<div class="variable-width" contenteditable="true" placeholder="email@address.com"></div>')
          .on('change keyup', fn.updateEmail)

        $(settings.el.label).next().find('.select2-selection, input').on('focus', fn.inputFocus);

        fn.inputFocus();
        fn.initQuantity();
      },

      updateEmail: function () {
        var email = $('.variable-width').html();
        $(settings.el.email).val( $('.variable-width').html() )
      },

      inputFocus: function (e) {
        $(settings.el.label).each(function(i, el) {
          if( $(el).closest('.contact__column').find('select, input').val() == '' ) $(el).removeClass('active');
          else $(el).addClass('active');
        });

        if(e) $(e.currentTarget).closest('.contact__column').find('label').addClass('active');
      },

      initQuantity: function () {
        // Number fields
        $('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('.quantity input');

        $('.quantity').each(function() {
          var spinner = $(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min') ? input.attr('min') : 0,
            max = input.attr('max') ? input.attr('max') : 100000;

          btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
              var newVal = oldValue;
            } else {
              var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
          });

          btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
              var newVal = oldValue;
            } else {
              var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
          });

        });
      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.insta = function( options ) {

    var settings = {
      el: {
        insta: ".insta",
      },
    },

    fn = {
      init: function () {

        var token = '2253179494.1677ed0.ceed275bfb3543a895fbf8b21b1d8a1c',
            // userid = 293099794, // User ID - get it in source HTML of your Instagram profile or look at the next example :)
            num_photos = 10;

        $.ajax({
          // url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent', // or /users/self/media/recent for Sandbox
          url: 'https://api.instagram.com/v1/users/self/media/recent',
          dataType: 'jsonp',
          type: 'GET',
          data: {access_token: token, count: num_photos},
          success: function(data){
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              var date = new Date(data[i].created_time * 1000);
              var image_url = data[i].images.thumbnail.url.replace('150x150', '320x320');
              $(settings.el.insta).append('<li><a href="'+data[i].link+'" target="_blank"><img src="'+image_url+'" alt="'+data[i].caption.text.replace(/"/g, '&quot;')+'"><div class="insta__content"><span class="insta__date">'+fn.formatDate(date)+'</span><span class="insta__likes">'+data[i].likes.count+'</span></div></a></li>');
            }

            $('.insta').slick({
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 5,
              swipeToSlide: true,
              arrows: false,
              autoplay: true,
              autoplaySpeed: 3000,
              responsive: [
                {
                  breakpoint: 1366,
                  settings: {
                    slidesToShow: 4,
                  }
                },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
          },
          error: function(data){
            console.log(data);
          }
        });


      },

      formatDate: function(date) {
        var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var day = date.getDate();
        var monthIndex = date.getMonth();

        return monthNames[monthIndex] + ' ' + day;
      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.moduleDemo = function( options ) {

    var settings = {
      setting: ''
    },

    fn = {
      init: function (e) {
        console.log('init')
      },
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.navigation = function( options ) {

    var settings = {
      el: {
        mainNav: '.main-navigation',
        handheldNav: '.handheld-navigation',
        headerNav: '.header-bottom-wrap',
        headerTop: '.header-top-wrap',
      }
    },

    fn = {
      init: function (e) {
        $(settings.el.mainNav).on('click', fn.toggleNav);

        // For when the middle of the page hits the top
        // var waypoint = new Waypoint({
        //   element: $(settings.el.headerTop),
        //   handler: function(direction) {
        //     console.log('fix');
        //     if(direction == 'down') $('body').addClass('fixed-header');
        //     else $('body').removeClass('fixed-header');
        //   },
        //   offset: function() {
        //     return -$(settings.el.headerTop).innerHeight();
        //   }
        // })
      },

      toggleNav: function () {
        $('body').attr('data-handheld-nav', $('body').attr('data-handheld-nav') == 'hidden' ? 'visible' : 'hidden');
        // $(settings.el.handheldNav).slideToggle();

      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.offcanvas = function( options ) {

    var settings = {
      el: {
        viewCart: '.view-cart-contents',
        cartContent: '.widget_shopping_cart',
        viewContact: '.contact-us-trigger',
        contactContent: '.contact-us',
        viewIconGuide: '.icon-guide-trigger',
        iconGuideContent: '.icon-guide',
        bg: '.offcanvas-bg',
      }
    },

    fn = {
      init: function () {
        // Cart
        $(settings.el.viewCart).on('click touchstart', fn.toggleCart);
        $(settings.el.cartContent).on('click touchstart', '.close-cart', fn.toggleCart);

        // Icon guide
        $(settings.el.viewIconGuide).on('click touchstart', fn.toggleIconGuide);
        $(settings.el.iconGuideContent).on('click touchstart', '.close-icon-guide', fn.toggleIconGuide);

        // Contact us
        $("body").on('click', "a[href*=wpcf7_contact_form]", function (e) {
          e.preventDefault();
          fn.toggleContact();
        });

        $(settings.el.viewContact).on('click touchstart', fn.toggleContact);
        $(settings.el.contactContent).on('click touchstart', '.close-contact', fn.toggleContact);


        // Close all
        $(settings.el.bg).on('click touchstart', fn.close);
      },

      toggleCart: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-cart-state', $('body').attr('data-cart-state') == 'hidden' ? 'visible' : 'hidden');
      },

      toggleContact: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-contact-state', $('body').attr('data-contact-state') == 'hidden' ? 'visible' : 'hidden');
      },

      toggleIconGuide: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-icon-guide-state', $('body').attr('data-icon-guide-state') == 'hidden' ? 'visible' : 'hidden');
      },

      close: function () {
        $('body').attr('data-cart-state', 'hidden')
          .attr('data-contact-state', 'hidden')
          .attr('data-icon-guide-state', 'hidden');
      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.sliders = function( options ) {

    var settings = {
      el: {
        testimonials: ".testimonials",
        hero: ".hero-home__slider ",
        people: ".people--slider",
        slide: ".testimonials > li",
        imageSlider: ".image-slider"
      },
    },

    fn = {
      init: function () {

        $(settings.el.testimonials).slick({
          arrows: false,
          dots: true,
          fade: true,
          autoplay: true,
          autoplaySpeed: 6000,
        });

        $(settings.el.hero).on('init', function(slick) {
          $(settings.el.hero).addClass('initialized');
        }).slick({
          arrows: false,
          dots: true,
          fade: true,
          autoplay: true,
          autoplaySpeed: 5000,
          pauseOnHover: false,
          draggable: false,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                dots: false,
              },
            },
          ]
        });

        //.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        //  $(settings.el.hero).find('[data-slick-index="' + currentSlide + '"]').fadeTo('fast',0);
        //});

        $(settings.el.people).slick({
          arrows: false,
          dots: true,
          fade: true
        });

        $(settings.el.imageSlider).slick({
          arrows: true,
          dots: true,
          prevArrow: '<button type="button" class="slick-prev"><i class="ion ion-ios-arrow-back"></i></button>',
          nextArrow: '<button type="button" class="slick-next"><i class="ion ion-ios-arrow-forward"></i></button>',
        });

      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.tabs = function( options ) {

    var settings = {
      el: {
        anchor: '.tabs a',
        content: '.tabs-content > div'
      }
    },

    fn = {
      init: function (e) {
        $(settings.el.anchor).on('click', fn.tabnav);
      },

      tabnav: function (e) {
        e.preventDefault();

        var link = $(e.currentTarget);

        // Update the link
        $(settings.el.anchor).removeClass('tab-active');
        link.addClass('tab-active');

        // Switch sections
        $(settings.el.content).removeClass('tab-active');
        $('[data-rel="' + link.attr('data-id') + '"]').addClass('tab-active');

        fn.refresh();
      },

      refresh: function () {
        Waypoint.refreshAll();
      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.waypoints = function( options ) {

    var settings = {
      el: {
        timeline: ".timeline__list",
        icon: ".timeline__icon",
        even: "ul .timeline__row:nth-child(2n + 1)",
        odd: "ul .timeline__row:nth-child(2n)",
        last: "ul .timeline__row:last-child",
      },
      elOffset: 50
    },

    fn = {
      init: function () {


        // For when the middle of the page hits the top
        var section = $('section');

        for (var i = 0; i < section.length; i++) {
          new Waypoint({
            element: section[i],
            handler: function(direction) {
              $(this.element).addClass('in-view');
            },
            offset: function() {
              return (window.innerHeight/1.75) - settings.elOffset
            }
          })
        }




        if($(settings.el.timeline).length == 0) return;

        // For when the middle of the page hits the top
        // var waypoint = new Waypoint({
        //   element: $(settings.el.timeline),
        //   handler: function(direction) {
        //     console.log('top');
        //     $(settings.el.icon).toggleClass('is-below-top');
        //   },
        //   offset: function() {
        //     return (window.innerHeight/2) - settings.elOffset
        //   }
        // })



        // For when the middle of the page hits a row of content == window.innerHeight/2
        var even = $(settings.el.even);
        var odd = $(settings.el.odd);
        var last = $(settings.el.last);

        for (var i = 0; i < even.length; i++) {
          new Waypoint({
            element: even[i],
            handler: function(direction) {

              if (direction == 'down') {
                $(settings.el.icon).removeClass('timeline__icon--left');
                var top = $(this.element).position().top + 50;
                $(settings.el.icon).css('top', top);
              } else {
                if ( $(this.element).prev().length > 0 ) {
                  $(settings.el.icon).addClass('timeline__icon--left');
                  var top = $(this.element).prev().position().top + 50;
                } else {
                  var top = 50;
                }
                $(settings.el.icon).css('top', top);
              }

              $(settings.el.icon).removeClass("timeline__icon--anim");
              void $(settings.el.icon).width();
              $(settings.el.icon).addClass("timeline__icon--anim");

            },
            offset: function() {
              return (window.innerHeight/1.75);// - $(this.element).innerHeight();
            }
          })
        }
        for (var i = 0; i < odd.length; i++) {
          new Waypoint({
            element: odd[i],
            handler: function(direction) {
              
              if (direction == 'down') {
                $(settings.el.icon).addClass('timeline__icon--left');
                var top = $(this.element).position().top + 50;
                $(settings.el.icon).css('top', top);
              } else {
                if ( $(this.element).prev().length > 0 ) {
                  $(settings.el.icon).removeClass('timeline__icon--left');
                  var top = $(this.element).prev().position().top + 50;
                } else {
                  var top = 50;
                }
                $(settings.el.icon).css('top', top);
              }
              
              $(settings.el.icon).removeClass("timeline__icon--anim");
              void $(settings.el.icon).width();
              $(settings.el.icon).addClass("timeline__icon--anim");

            },
            offset: function() {
              return (window.innerHeight/1.75);// - $(this.element).innerHeight();
            }
          })
        }


        // For when the middle of the page hits the bottom
        var waypoint2 = new Waypoint({
          element: $(settings.el.timeline),
          handler: function(direction) {
            // console.log('bottom');
            if (direction == 'down') {
              $(settings.el.icon).toggleClass('is-below-end');
              $(settings.el.icon).css('top', $(settings.el.timeline).innerHeight() + 30);
            } else {
              var top = $(settings.el.last).position().top + 50;
              $(settings.el.icon).css('top', top);
            }

            $(settings.el.icon).removeClass("timeline__icon--anim");
            void $(settings.el.icon).width();
            $(settings.el.icon).addClass("timeline__icon--anim");

          },
          offset: function() {
            return (window.innerHeight/1.75) - $(settings.el.timeline).innerHeight() + $(settings.el.icon).innerHeight() - 80
          }
        })

      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);

/**
 * Custom JS.
 *
 * Custom JS scripts.
 *
 * @since 1.0.0
 */
var duffle = duffle || {};

(function ($) {

  // duffle.moduleDemo();
  // duffle.utilityDemo().func2();

  duffle.featuredProducts();
  duffle.insta();
  duffle.forms();
  duffle.offcanvas();
  duffle.navigation();
  duffle.tabs();
  duffle.sliders();
  duffle.accordion();
  duffle.waypoints();
  duffle.footer();

})(jQuery);
