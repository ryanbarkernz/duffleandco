var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.navigation = function( options ) {

    var settings = {
      el: {
        mainNav: '.main-navigation',
        handheldNav: '.handheld-navigation',
        headerNav: '.header-bottom-wrap',
        headerTop: '.header-top-wrap',
      }
    },

    fn = {
      init: function (e) {
        $(settings.el.mainNav).on('click', fn.toggleNav);

        // For when the middle of the page hits the top
        // var waypoint = new Waypoint({
        //   element: $(settings.el.headerTop),
        //   handler: function(direction) {
        //     console.log('fix');
        //     if(direction == 'down') $('body').addClass('fixed-header');
        //     else $('body').removeClass('fixed-header');
        //   },
        //   offset: function() {
        //     return -$(settings.el.headerTop).innerHeight();
        //   }
        // })
      },

      toggleNav: function () {
        $('body').attr('data-handheld-nav', $('body').attr('data-handheld-nav') == 'hidden' ? 'visible' : 'hidden');
        // $(settings.el.handheldNav).slideToggle();

      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
