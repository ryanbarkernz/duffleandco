var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.offcanvas = function( options ) {

    var settings = {
      el: {
        viewCart: '.view-cart-contents',
        cartContent: '.widget_shopping_cart',
        viewContact: '.contact-us-trigger',
        contactContent: '.contact-us',
        viewIconGuide: '.icon-guide-trigger',
        iconGuideContent: '.icon-guide',
        bg: '.offcanvas-bg',
      }
    },

    fn = {
      init: function () {
        // Cart
        $(settings.el.viewCart).on('click touchstart', fn.toggleCart);
        $(settings.el.cartContent).on('click touchstart', '.close-cart', fn.toggleCart);

        // Icon guide
        $(settings.el.viewIconGuide).on('click touchstart', fn.toggleIconGuide);
        $(settings.el.iconGuideContent).on('click touchstart', '.close-icon-guide', fn.toggleIconGuide);

        // Contact us
        $("body").on('click', "a[href*=wpcf7_contact_form]", function (e) {
          e.preventDefault();
          fn.toggleContact();
        });

        $(settings.el.viewContact).on('click touchstart', fn.toggleContact);
        $(settings.el.contactContent).on('click touchstart', '.close-contact', fn.toggleContact);


        // Close all
        $(settings.el.bg).on('click touchstart', fn.close);
      },

      toggleCart: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-cart-state', $('body').attr('data-cart-state') == 'hidden' ? 'visible' : 'hidden');
      },

      toggleContact: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-contact-state', $('body').attr('data-contact-state') == 'hidden' ? 'visible' : 'hidden');
      },

      toggleIconGuide: function (e) {
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        $('body').attr('data-icon-guide-state', $('body').attr('data-icon-guide-state') == 'hidden' ? 'visible' : 'hidden');
      },

      close: function () {
        $('body').attr('data-cart-state', 'hidden')
          .attr('data-contact-state', 'hidden')
          .attr('data-icon-guide-state', 'hidden');
      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
