var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.moduleDemo = function( options ) {

    var settings = {
      setting: ''
    },

    fn = {
      init: function (e) {
        console.log('init')
      },
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
