var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.insta = function( options ) {

    var settings = {
      el: {
        insta: ".insta",
      },
    },

    fn = {
      init: function () {

        var token = '2253179494.1677ed0.ceed275bfb3543a895fbf8b21b1d8a1c',
            // userid = 293099794, // User ID - get it in source HTML of your Instagram profile or look at the next example :)
            num_photos = 10;

        $.ajax({
          // url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent', // or /users/self/media/recent for Sandbox
          url: 'https://api.instagram.com/v1/users/self/media/recent',
          dataType: 'jsonp',
          type: 'GET',
          data: {access_token: token, count: num_photos},
          success: function(data){
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              var date = new Date(data[i].created_time * 1000);
              var image_url = data[i].images.thumbnail.url.replace('150x150', '320x320');
              $(settings.el.insta).append('<li><a href="'+data[i].link+'" target="_blank"><img src="'+image_url+'" alt="'+data[i].caption.text.replace(/"/g, '&quot;')+'"><div class="insta__content"><span class="insta__date">'+fn.formatDate(date)+'</span><span class="insta__likes">'+data[i].likes.count+'</span></div></a></li>');
            }

            $('.insta').slick({
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 5,
              swipeToSlide: true,
              arrows: false,
              autoplay: true,
              autoplaySpeed: 3000,
              responsive: [
                {
                  breakpoint: 1366,
                  settings: {
                    slidesToShow: 4,
                  }
                },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
          },
          error: function(data){
            console.log(data);
          }
        });


      },

      formatDate: function(date) {
        var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var day = date.getDate();
        var monthIndex = date.getMonth();

        return monthNames[monthIndex] + ' ' + day;
      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
