var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.tabs = function( options ) {

    var settings = {
      el: {
        anchor: '.tabs a',
        content: '.tabs-content > div'
      }
    },

    fn = {
      init: function (e) {
        $(settings.el.anchor).on('click', fn.tabnav);
      },

      tabnav: function (e) {
        e.preventDefault();

        var link = $(e.currentTarget);

        // Update the link
        $(settings.el.anchor).removeClass('tab-active');
        link.addClass('tab-active');

        // Switch sections
        $(settings.el.content).removeClass('tab-active');
        $('[data-rel="' + link.attr('data-id') + '"]').addClass('tab-active');

        fn.refresh();
      },

      refresh: function () {
        Waypoint.refreshAll();
      }
    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
