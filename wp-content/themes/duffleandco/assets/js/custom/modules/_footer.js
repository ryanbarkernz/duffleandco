var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.footer = function( options ) {

    var settings = {
      el: {
      },
    },

    fn = {
      init: function () {
        $(window).on('resize', fn.fixFooter);
        fn.fixFooter();
      },

      fixFooter: function () {
        if ($('.footer .container').innerHeight() + 200 < window.innerHeight) {
          $('body').attr('data-footer', 'fixed');
        } else {
          $('body').attr('data-footer', 'default');
        }
      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
