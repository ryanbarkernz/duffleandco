var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.waypoints = function( options ) {

    var settings = {
      el: {
        timeline: ".timeline__list",
        icon: ".timeline__icon",
        even: "ul .timeline__row:nth-child(2n + 1)",
        odd: "ul .timeline__row:nth-child(2n)",
        last: "ul .timeline__row:last-child",
      },
      elOffset: 50
    },

    fn = {
      init: function () {


        // For when the middle of the page hits the top
        var section = $('section');

        for (var i = 0; i < section.length; i++) {
          new Waypoint({
            element: section[i],
            handler: function(direction) {
              $(this.element).addClass('in-view');
            },
            offset: function() {
              return (window.innerHeight/1.75) - settings.elOffset
            }
          })
        }




        if($(settings.el.timeline).length == 0) return;

        // For when the middle of the page hits the top
        // var waypoint = new Waypoint({
        //   element: $(settings.el.timeline),
        //   handler: function(direction) {
        //     console.log('top');
        //     $(settings.el.icon).toggleClass('is-below-top');
        //   },
        //   offset: function() {
        //     return (window.innerHeight/2) - settings.elOffset
        //   }
        // })



        // For when the middle of the page hits a row of content == window.innerHeight/2
        var even = $(settings.el.even);
        var odd = $(settings.el.odd);
        var last = $(settings.el.last);

        for (var i = 0; i < even.length; i++) {
          new Waypoint({
            element: even[i],
            handler: function(direction) {

              if (direction == 'down') {
                $(settings.el.icon).removeClass('timeline__icon--left');
                var top = $(this.element).position().top + 50;
                $(settings.el.icon).css('top', top);
              } else {
                if ( $(this.element).prev().length > 0 ) {
                  $(settings.el.icon).addClass('timeline__icon--left');
                  var top = $(this.element).prev().position().top + 50;
                } else {
                  var top = 50;
                }
                $(settings.el.icon).css('top', top);
              }

              $(settings.el.icon).removeClass("timeline__icon--anim");
              void $(settings.el.icon).width();
              $(settings.el.icon).addClass("timeline__icon--anim");

            },
            offset: function() {
              return (window.innerHeight/1.75);// - $(this.element).innerHeight();
            }
          })
        }
        for (var i = 0; i < odd.length; i++) {
          new Waypoint({
            element: odd[i],
            handler: function(direction) {
              
              if (direction == 'down') {
                $(settings.el.icon).addClass('timeline__icon--left');
                var top = $(this.element).position().top + 50;
                $(settings.el.icon).css('top', top);
              } else {
                if ( $(this.element).prev().length > 0 ) {
                  $(settings.el.icon).removeClass('timeline__icon--left');
                  var top = $(this.element).prev().position().top + 50;
                } else {
                  var top = 50;
                }
                $(settings.el.icon).css('top', top);
              }
              
              $(settings.el.icon).removeClass("timeline__icon--anim");
              void $(settings.el.icon).width();
              $(settings.el.icon).addClass("timeline__icon--anim");

            },
            offset: function() {
              return (window.innerHeight/1.75);// - $(this.element).innerHeight();
            }
          })
        }


        // For when the middle of the page hits the bottom
        var waypoint2 = new Waypoint({
          element: $(settings.el.timeline),
          handler: function(direction) {
            // console.log('bottom');
            if (direction == 'down') {
              $(settings.el.icon).toggleClass('is-below-end');
              $(settings.el.icon).css('top', $(settings.el.timeline).innerHeight() + 30);
            } else {
              var top = $(settings.el.last).position().top + 50;
              $(settings.el.icon).css('top', top);
            }

            $(settings.el.icon).removeClass("timeline__icon--anim");
            void $(settings.el.icon).width();
            $(settings.el.icon).addClass("timeline__icon--anim");

          },
          offset: function() {
            return (window.innerHeight/1.75) - $(settings.el.timeline).innerHeight() + $(settings.el.icon).innerHeight() - 80
          }
        })

      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
