var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.accordion = function( options ) {

    var settings = {
      el: {
        accordion: ".panels",
        toggle: ".panel",
        content: ".panels__content > div",
      },
      speed: 300,
      active: false
    },

    fn = {
      init: function () {

        if(!$(settings.el.accordion)) return;
        $(settings.el.toggle).on('click', fn.toggle);

      },

      toggle: function (e) {

        var $el = $(e.currentTarget),
            id = '#' + $el.attr('data-id'),
            $next = $(id);

        // Toggle data state on the accordion toggle
        if ( $el.attr('data-state') === 'active' ) {
          $el.attr('data-state', '');
          $(settings.el.accordion).attr('data-state', 'closed');
        } else {
          $(settings.el.toggle).attr('data-state', '');
          $el.attr('data-state', 'active');
          $(settings.el.accordion).attr('data-state', 'open');
        }

        // Expand or collapse this accordion content
        $next.addClass('is-active').slideToggle(settings.speed, fn.refresh);

        if($next.find('.image-slider').length !== 0) $next.find('.image-slider').get(0).slick.setPosition();

        // Hide the other panels
        $(settings.el.content).not($next).removeClass('is-active').slideUp(settings.speed);

      },

      refresh: function () {
        Waypoint.refreshAll();
      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
