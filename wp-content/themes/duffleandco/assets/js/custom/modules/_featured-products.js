var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.featuredProducts = function( options ) {

    var settings = {
      el: {
        category: ".fp__categories > a",
        product: ".fp__products > li",
      },
      id: 'all',
    },

    fn = {
      init: function () {

        $(settings.el.category).on('click', fn.filter);
        settings.limited = $('.featured-products--limited').length > 0;

        settings.id = $(settings.el.category).length > 0 ? $(settings.el.category).first().attr('data-id') : 'all';

        fn.filter();

        $('.woocommerce-product-gallery').find('.flex-control-thumbs img').after('<div></div>');

      },

      filter: function (e) {

        if(e) {
          e.preventDefault();
          var $el = $(e.currentTarget);
          settings.id = $el.attr('data-id');
        }

        $(settings.el.category).removeClass('active');
        $('[data-id="' + settings.id + '"]').addClass('active');

        var i = 0;

        $(settings.el.product).each(function () {
          if($(this).hasClass('fp__link')) return $(this).attr('data-visibility', 'visible').attr('data-order', i);

          $(this).attr('data-visibility', 'hidden').attr('data-order', -1).removeClass('fp__product--offset');

          if (i < 3 || !settings.limited) {
            if (settings.id == 'all') {
              $(this).attr('data-visibility', 'visible').attr('data-order', i);
              i++;
            } else if ($(this).attr('data-cat').indexOf(settings.id) >= 0) {
              $(this).attr('data-visibility', 'visible').attr('data-order', i);
              i++;
            }
            if(i % 2 !== 0) $(this).addClass('fp__product--offset');
          }

        })

      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
