var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.sliders = function( options ) {

    var settings = {
      el: {
        testimonials: ".testimonials",
        hero: ".hero-home__slider ",
        people: ".people--slider",
        slide: ".testimonials > li",
        imageSlider: ".image-slider"
      },
    },

    fn = {
      init: function () {

        $(settings.el.testimonials).slick({
          arrows: false,
          dots: true,
          fade: true,
          autoplay: true,
          autoplaySpeed: 6000,
        });

        $(settings.el.hero).on('init', function(slick) {
          $(settings.el.hero).addClass('initialized');
        }).slick({
          arrows: false,
          dots: true,
          fade: true,
          autoplay: true,
          autoplaySpeed: 5000,
          pauseOnHover: false,
          draggable: false,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                dots: false,
              },
            },
          ]
        });

        //.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        //  $(settings.el.hero).find('[data-slick-index="' + currentSlide + '"]').fadeTo('fast',0);
        //});

        $(settings.el.people).slick({
          arrows: false,
          dots: true,
          fade: true
        });

        $(settings.el.imageSlider).slick({
          arrows: true,
          dots: true,
          prevArrow: '<button type="button" class="slick-prev"><i class="ion ion-ios-arrow-back"></i></button>',
          nextArrow: '<button type="button" class="slick-next"><i class="ion ion-ios-arrow-forward"></i></button>',
        });

      }

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
