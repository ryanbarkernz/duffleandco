var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.forms = function( options ) {

    var settings = {
      el: {
        selects: '#content select',
        contactSelect: '.contact-us .wpcf7 select',
        footerSelect: '.footer .wpcf7 select',
        email: '.wpcf7 input[type="email"]',
        variableEmailWrap: '.footer .wpcf7 .mc4wp-EMAIL',
        label: 'label.placeholder',
        numbers: 'label.placeholder',
      },
    },

    fn = {
      init: function () {

        $(settings.el.footerSelect).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: 'auto',
        }).data('select2').$dropdown.addClass('footer-select-dropdown');

        $(settings.el.selects).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: '100%',
        }).focus(function () { $(this).select2('focus'); });

        $(settings.el.contactSelect).select2({
          minimumResultsForSearch: 10,
          dropdownAutoWidth: true,
          width: '100%',
        }).focus(function () { $(this).select2('focus'); })
          .data('select2').$dropdown.addClass('contact-select-dropdown');

        // $(settings.el.email).on('keyup', fn.updateEmailLength)
        $(settings.el.variableEmailWrap)
          .append('<div class="variable-width" contenteditable="true" placeholder="email@address.com"></div>')
          .on('change keyup', fn.updateEmail)

        $(settings.el.label).next().find('.select2-selection, input').on('focus', fn.inputFocus);

        fn.inputFocus();
        fn.initQuantity();
      },

      updateEmail: function () {
        var email = $('.variable-width').html();
        $(settings.el.email).val( $('.variable-width').html() )
      },

      inputFocus: function (e) {
        $(settings.el.label).each(function(i, el) {
          if( $(el).closest('.contact__column').find('select, input').val() == '' ) $(el).removeClass('active');
          else $(el).addClass('active');
        });

        if(e) $(e.currentTarget).closest('.contact__column').find('label').addClass('active');
      },

      initQuantity: function () {
        // Number fields
        $('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('.quantity input');

        $('.quantity').each(function() {
          var spinner = $(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min') ? input.attr('min') : 0,
            max = input.attr('max') ? input.attr('max') : 100000;

          btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
              var newVal = oldValue;
            } else {
              var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
          });

          btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
              var newVal = oldValue;
            } else {
              var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
          });

        });
      },

    };

    /**********************************
    // Simple constructor
    **********************************/
    function __construct() {
      fn.init();
    }

    __construct();

  };

})(jQuery);
