/**
 * Custom JS.
 *
 * Custom JS scripts.
 *
 * @since 1.0.0
 */
var duffle = duffle || {};

(function ($) {

  // duffle.moduleDemo();
  // duffle.utilityDemo().func2();

  duffle.featuredProducts();
  duffle.insta();
  duffle.forms();
  duffle.offcanvas();
  duffle.navigation();
  duffle.tabs();
  duffle.sliders();
  duffle.accordion();
  duffle.waypoints();
  duffle.footer();

})(jQuery);
