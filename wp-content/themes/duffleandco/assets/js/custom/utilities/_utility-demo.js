var duffle = duffle || {};

(function($) {
  'use strict';

  duffle.utilityDemo = function( options ) {

    var settings = {
      setting: ''
    },

    fn = {
      func: function (e) {
        console.log('func')
      },

      func2: function (e) {
        console.log('func2')
      },
    };

    //
    // Make some events accessible from global scope
    //
    return {
      func: fn.func,
      func2: fn.func2
    };
  };


})(jQuery);

// usage: duffle.utilityDemo().func1()
