/*

  extra Math functions

*/

(function ($) {

  $("body").on('click', "a[href^=#]", function (e) {
    e.preventDefault();

    var $el = $($(this).attr('href'));
    if(!$el.length) return;

    $('html,body').animate({scrollTop: $el.offset().top}, 400);
  });

})(jQuery);
