<?php
/**
 * The template part for displaying the builder partials
 */
global $pageid;

// check if the flexible content field has rows of data
if( have_rows('page_builder', $pageid) ):

  $i = 1;

  // loop through the rows of data
  while ( have_rows('page_builder', $pageid) ) : the_row();

    echo '<div id="section-'.$i.'"></div>';

    if( get_row_layout() == 'builder_page_anchors' ) get_template_part('inc/partials/builder/_anchors');

    if( get_row_layout() == 'builder_text_block' ) get_template_part('inc/partials/builder/_text-block');

    if( get_row_layout() == 'builder_custom' ) get_template_part('inc/partials/builder/_custom');

    if( get_row_layout() == 'builder_cta_block_1' ) get_template_part('inc/partials/builder/_cta-block-1');

    if( get_row_layout() == 'builder_panel_accordion' ) get_template_part('inc/partials/builder/_panel-accordion');

    if( get_row_layout() == 'builder_cta_block_2' ) get_template_part('inc/partials/builder/_cta-block-2');

    if( get_row_layout() == 'builder_featured_products' ) get_template_part('inc/partials/builder/_featured-products');

    if( get_row_layout() == 'builder_social_insta_slider' ) get_template_part('inc/partials/builder/_social-feed');

    if( get_row_layout() == 'builder_testimonial_slider' ) get_template_part('inc/partials/builder/_testimonials');

    if( get_row_layout() == 'builder_timeline' ) get_template_part('inc/partials/builder/_timeline');

    if( get_row_layout() == 'builder_initiatives' ) get_template_part('inc/partials/builder/_initiatives');

    if( get_row_layout() == 'builder_people' ) get_template_part('inc/partials/builder/_people');

    $i++;

  endwhile;

else :
    // no layouts found
endif;
?>
