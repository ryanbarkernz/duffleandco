<section class="bg-block">
  <div class="bg-block__bg"></div>
  <div class="container content-box">
    <div class="grid">
      <div class="grid__column grid__column--5">
        <h2>Lorem ipsum dolor sit amet, consectetur</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
        <div class="bg-block__buttons">
          <a href="#" class="arrow-btn">Our Products</a>
          <a href="#" class="arrow-btn">Lorem ipsum</a>
        </div>
      </div>
      <div class="grid__column grid__column--7">
        <div class="bg-block__image">
          <img src="http://placehold.it/900x650" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
