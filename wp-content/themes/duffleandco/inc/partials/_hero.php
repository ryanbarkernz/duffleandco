<?php
global $pageid;
?>
<section>
  <div class="hero-home">
    <?php if( get_field('hero_images', $pageid) ): ?>
    <div class="hero-home__slider">
      <?php foreach( get_field('hero_images', $pageid) as $image ) :
      $hero_style = 'background-image: url('.wp_get_attachment_image_src($image['ID'], 'large')[0].');';
      ?>
      <div class="slider__image" style="<?= $hero_style ?>"></div>
      <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="container container--tablet">
      <div class="content-box">
        <?php if ( get_field('hero_heading', $pageid) ) : ?><h1><?= get_field('hero_heading', $pageid); ?></h1><?php endif; ?>
        <?php if ( get_field('hero_text', $pageid) ) : ?><p><?= get_field('hero_text', $pageid); ?></p><?php endif; ?>
        <?php if ( have_rows('hero_buttons', $pageid) ): ?>
        <div class="slider__buttons">
          <?php
            while ( have_rows('hero_buttons', $pageid) ) : the_row();
              $hash = get_sub_field('hero_button_link_to_section') ? '#section-' . get_sub_field('hero_button_link_to_section') : '';
              $url = get_sub_field('hero_button_url') . $hash;
              $target = get_sub_field('hero_button_type') == 'Page' ? '_self' : '_blank';
              ?>
          <a href="<?= $url; ?>" class="text-btn" target="<?= $target; ?>"><?= get_sub_field('hero_button_text'); ?></a>
          <?php endwhile; ?>
        </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>
