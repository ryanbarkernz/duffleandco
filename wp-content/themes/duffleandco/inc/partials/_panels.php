<section class="vertical has-bg-color" style="background-color: #f7f7f7;">
  <div class="container">

    <div class="grid-stretched">

      <div>
        <h1 class="vertical__heading">Our values</h1>
      </div>

      <div class="panels">

        <div class="panel__titles">
          <div class="panel" style="background-color: #396f81;" data-id="panel1">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/eye.gif')"></div>
                <div style="background-image: url('/ui/images/eye-anim.gif')"></div>
              </div>
              <span>Transparency</span>
            </div>
          </div>
          <div class="panel" style="background-color: #3a4b7e;" data-id="panel2">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/scale.gif')"></div>
                <div style="background-image: url('/ui/images/scale-anim.gif')"></div>
              </div>
              <span>Ethical Labour</span>
            </div>
          </div>
          <div class="panel panel--primary" style="background-color: #f5f0e5;" data-id="panel3">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/craftsmanship.gif')"></div>
                <div style="background-image: url('/ui/images/craftsmanship-anim.gif')"></div>
              </div>
              <span>Craftmanship</span>
            </div>
          </div>
        </div>
        <div class="panels__content">
          <div id="panel1">test1</div>
          <div id="panel2">test2</div>
          <div id="panel3">test3</div>
        </div>

        <div class="panel__titles">
          <div class="panel" style="background-color: #c6a583;" data-id="panel4">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/heart.gif')"></div>
                <div style="background-image: url('/ui/images/heart-anim.gif')"></div>
              </div>
              <span>Inclusivity</span>
            </div>
          </div>
          <div class="panel" style="background-color: #c7c7c7;" data-id="panel5">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/mountain.gif')"></div>
                <div style="background-image: url('/ui/images/mountain-anim.gif')"></div>
              </div>
              <span>Nature</span>
            </div>
          </div>
          <div class="panel panel--primary" style="background-color: #ffffff;" data-id="panel6">
            <span class="panel__toggle"></span>
            <div class="panel__content">
              <div class="panel__image">
                <div style="background-image: url('/ui/images/kiwi.gif')"></div>
                <div style="background-image: url('/ui/images/kiwi-anim.gif')"></div>
              </div>
              <span>Initiatives</span>
            </div>
          </div>
        </div>
        <div class="panels__content">
          <div id="panel4">test1</div>
          <div id="panel5">test2</div>
          <div id="panel6">test3</div>
        </div>

      </div>

      <div class="vertical__buttons">
        <div>
          <a href="#" class="arrow-btn arrow-btn--vertical">Our Products</a>
          <a href="#" class="arrow-btn arrow-btn--vertical">Lorem ipsum</a>
        </div>
      </div>

    </div>

  </div>
</section>
