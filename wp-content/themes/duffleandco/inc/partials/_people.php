<section class="vertical has-bg-color" style="background-color: #f6f0e4;">
  <div class="container">

    <div class="grid-stretched">

      <div>
        <h1 class="vertical__heading">Craftsman</h1>
      </div>

      <ul class="people">
        <li class="person">
          <div class="person__image" style="background-image: url('/ui/images/eye.gif')"></div>
          <p class="person__name">Danny Pritchard</p>
          <p class="person__title">Title</p>
        </li>
      </ul>

      <div class="vertical__buttons">
        <div>
          <a href="#" class="arrow-btn arrow-btn--vertical">Our Products</a>
          <a href="#" class="arrow-btn arrow-btn--vertical">Lorem ipsum</a>
        </div>
      </div>

    </div>

  </div>
</section>
