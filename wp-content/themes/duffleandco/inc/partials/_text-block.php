<section>
  <div class="container">
    <div class="text-block text-block--reverse">
      <div class="text-block__text">
        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>
      </div>
      <div class="text-block__image">
        <img src="/ui/images/whale-mountain-goat.png" alt="">
      </div>
    </div>
  </div>
</section>
