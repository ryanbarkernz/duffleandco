<section class="has-bg-color" style="background-color: #f7f7f7;">
  <div class="container">

    <div class="social__header">
      <?php if ( get_field('instagram', 'option') ) : ?>
      <h2><a href="<?= get_field('instagram', 'option'); ?>" target="_blank">Follow us</a></h2>
      <?php else : ?>
      <h2>Follow us</h2>
      <?php endif; ?>

      <div class="vertical__buttons">
        <div class="vb_wrap">
          <a href="/shop" class="arrow-btn arrow-btn--vertical"><span>The Shop</span></a>
        </div>
      </div>
    </div>

  </div>

  <ul class="insta"></ul>

  <div class="container">
    <div class="social__footer">
      <?php if ( get_field('instagram', 'option') ) :
        $url = get_field('instagram', 'option');
        $name = end( array_filter(explode('/', $url)) ); ?>
      <a href="<?= $url; ?>" target="_blank" class="instagram"><i class="icon-social-instagram icons"></i> @<?= $name; ?></a><?php endif; ?>
      <?php if ( get_field('facebook', 'option') ) :
        $url = get_field('facebook', 'option');
        $name = end( array_filter(explode('/', $url)) ); ?>
      <a href="<?= $url; ?>" target="_blank" class="facebook"><i class="icon-social-facebook icons"></i> /<?= $name; ?></a><?php endif; ?>
    </div>
  </div>

</section>
