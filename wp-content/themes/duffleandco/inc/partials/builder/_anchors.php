<?php
if( have_rows('anchors') ): ?>
<div class="anchors">
  <div class="container">
    <ul>
    <?php
      while ( have_rows('anchors') ) : the_row();?>
      <li><a href="#section-<?= get_sub_field('anchor_id'); ?>"><?= get_sub_field('anchor_text'); ?></a></li>
    <?php endwhile; ?>
    </ul>
  </div>
</div>
<?php endif; ?>
