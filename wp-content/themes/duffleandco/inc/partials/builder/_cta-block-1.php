<?php
/**
 * The template part for displaying the text block
 */

// vars

$heading = get_sub_field('heading');
$text = get_sub_field('text');
$buttons = get_sub_field('buttons');
$image = get_sub_field('image');

$classes[] = 'bg-block';

$classes = $classes ? implode(' ', $classes ) : '';

?>
<section class="<?= $classes ?>">
  <div class="bg-block__bg"></div>
  <div class="container content-box">
    <div class="grid">
      <div class="grid__column grid__column--5">
        <h2><?= $heading ?></h2>
        <p><?= $text ?></p>
        <?php
        if( have_rows('buttons') ): ?>
        <div class="bg-block__buttons">
        <?php
          while ( have_rows('buttons') ) : the_row();
            $hash = get_sub_field('button_link_to_section') ? '#section-' . get_sub_field('button_link_to_section') : '';
            $url = get_sub_field('button_url') . $hash;
            $target = get_sub_field('button_type') == 'Page' ? '_self' : '_blank';
            ?>
            <a href="<?= $url ?>" class="arrow-btn" target="<?= $target ?>"><?= get_sub_field('button_text'); ?></a>
          <?php endwhile; ?>
        </div>
        <?php endif; ?>
      </div>
      <div class="grid__column grid__column--7">
        <div class="bg-block__image">
          <img src="<?= $image['sizes']['large']; ?>" alt="<?= $image['alt'] ?>">
        </div>
      </div>
    </div>
  </div>
</section>
