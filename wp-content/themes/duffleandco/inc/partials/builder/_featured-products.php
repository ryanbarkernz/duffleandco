<?php
$heading = get_sub_field('heading') ? get_sub_field('heading') : 'Featured Products';
?>
<section class="featured-products featured-products--limited">
  <div class="container">

    <div class="fp__header">
      <div>
        <h2><?= $heading ?></h2>

        <div class="fp__categories">
          <?php

            $args = array(
              'taxonomy'     => 'product_cat',
              'orderby'      => 'name',
              'show_count'   => 0,
              'pad_counts'   => 0,
              'hierarchical' => 1,
              'title_li'     => '',
              'hide_empty'   => 0
            );
            $categories = get_categories( $args );
            foreach ($categories as $cat) {
              if($cat->category_parent == 0) {
                $category_id = $cat->term_id;
                echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'" data-id="' . $category_id . '">'. $cat->name .'</a>';
              }
          }
          ?>
        </div>
      </div>

      <div class="vertical__buttons">
        <div class="vb_wrap">
          <a href="/shop" class="arrow-btn arrow-btn--vertical"><span>The Shop</span></a>
        </div>
      </div>
    </div>

    <?php

    $featured_query = new WP_Query( array(
      'posts_per_page' => -1,
      'orderby' => 'menu_order',
      'order'   => 'ASC',
      'tax_query' => array(
      array(
        'taxonomy' => 'product_visibility',
        'field'    => 'name',
        'terms'    => 'featured',
        'operator' => 'IN',
        ),
      ),
    ));

    if ($featured_query->have_posts()) : ?>
    <ul class="fp__products">


      <?php while ($featured_query->have_posts()) :

        $featured_query->the_post();

        $product = get_product( $featured_query->post->ID );
        $initiative_icon = get_field('product_initiative_icon', $product->id);
        $initiative_text = get_field('product_initiative_text', $product->id);
        ?>

        <li class="fp__product" data-cat="<?= join(', ', $product->category_ids) ?>" data-visibility="visible">
          <a href="/product/<?= $product->slug ?>" class="product__image" style="background-image: url(<?= wp_get_attachment_image_src($product->image_id, 'large')[0]; ?>);">
            <div class="product__hover">
              <?php if ($initiative_icon) : ?><i class="duffleicon <?= $initiative_icon; ?>"></i><?php endif; ?>
              <?php if ($initiative_text) : ?><p><?= $initiative_text; ?></p><?php endif; ?>
              <div class="text-btn text-btn--sml text-btn--active">View product</div>
            </div>
          </a>
          <div class="product__description">
            <?php
            // FEATURE ICONS
            $product_features = get_field('product_features', $product->id) ? get_field('product_features', $product->id) : [];
            $product_packaging_stationary = get_field('product_packaging_stationary', $product->id) ? get_field('product_packaging_stationary', $product->id) : [];
            $features = array_merge($product_features, $product_packaging_stationary);
            if( $features ): ?>
            <ul class="product__features">
              <?php foreach( $features as $feature ): ?>
                <li>
                  <i class="duffleicon duffleicon--<?= strtolower(str_replace(' ', '-', $feature)); ?>"></i>
                  <div class="tooltip"><?= $feature ?></div>
                </li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>

            <h4><?= $product->name ?></h4>
            <p class="attributes"><?= str_replace(',', ' |', $product->get_attribute( 'pa_colour' )) ?></p>
            <div class="price">$<?= $product->price ?></div>

          </div>
        </li>

      <?php endwhile; ?>

      <li class="fp__link" style="background-color: #fff;">
        <a href="/shop" class="active">All products</a>
      </li>

    </ul>
    <?php endif;

    wp_reset_query();

    ?>

  </div>
</section>
