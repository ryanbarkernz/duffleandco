<?php

// Template Settings
$count = 0;
$tl_row = get_sub_field('timeline_rows');
$heading = get_sub_field('heading');
$tl_row_length = count($tl_row);
$last_row = end($tl_row);

if( have_rows('timeline_rows') ):
?>
<section>
  <div class="container">
    <?php if ($heading) : ?><h2 class="timeline-heading"><?= $heading ?></h2><?php endif; ?>

    <div class="timeline">
      <div class="timeline__list">
        <i class="duffleicon duffleicon--goat timeline__icon"></i>
        <div class="timeline__bar"></div>
        <ul>

          <?php while ( have_rows('timeline_rows') ) : the_row();
            $image = get_sub_field('image');
          ?>
            <li class="timeline__row">
              <div class="timeline__text">
                <h5><?= get_sub_field('title'); ?></h5>
                <p><?= get_sub_field('text'); ?></p>
              </div>
              <div class="timeline__image">
                <?php if ($image) : ?><img src="<?= $image['sizes']['medium'] ?>" alt="<?= $image['alt'] ?>" /><?php endif; ?>
              </div>
            </li>
          <?php
            $count++;
            if ( $count == ($tl_row_length - 1) ) break;
          endwhile; ?>

        </ul>
      </div>
      <div class="timeline__row">
        <div class="timeline__text">
          <h5><?= $last_row['title'] ?></h5>
          <p><?= $last_row['text'] ?></p>
        </div>
        <div class="timeline__image">
          <?php if ($last_row['image']) : ?><img src="<?= $last_row['image']['url']; ?>" alt="<?= $last_row['image']['alt'] ?>" /><?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif;
