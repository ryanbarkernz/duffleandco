<?php
/**
 * The template part for displaying the text block
 */

// Template Settings
$text = get_sub_field('text');
$imageLeft = get_sub_field('image_left');
$imageRight = get_sub_field('image_right');
$reverse = get_sub_field('reverse_row_order');
$layout = get_sub_field('style');

// Section settings
$settings = get_sub_field('section_settings');
$bg_colour = $settings['section_background_colour'];
$bg_image = $settings['section_background_image'];

if ($settings['section_text_colour'] !== 'default') $classes[] = $settings['section_text_colour'];

if($bg_colour) $classes[] = 'has-bg-color';
if($bg_image) $classes[] = 'has-bg-image';

if($bg_colour || $bg_image) {
  $style = $bg_image ? 'background-image: url('.$bg_image.');' : 'background-color: '.$bg_colour.';';
}

$classes = $classes ? implode(' ', $classes ) : '';

?>
<section class="<?= $classes ?>" style="<?= $style ?>">
 <div class="container">

  <?php if($layout == 'twoimg') : ?>

    <div class="text-block text-block--double<?php echo $reverse ? ' text-block--reverse' : ''; ?>">
      <div class="text-block__image">
        <img src="<?= $imageLeft['sizes']['large']; ?>" alt="<?= $imageLeft['alt'] ?>">
      </div>
      <div class="text-block__text">
        <h3><?= $text ?></h3>
      </div>
      <div class="text-block__image">
        <img src="<?= $imageRight['sizes']['large']; ?>" alt="<?= $imageRight['alt'] ?>">
      </div>
    </div>

 <?php elseif($layout == 'oneimg') : ?>

    <div class="text-block text-block--single<?php echo $reverse ? ' text-block--reverse' : ''; ?>">
      <div class="text-block__text">
        <h3><?= $text ?></h3>
      </div>
      <div class="text-block__image">
        <img src="<?= $imageLeft['sizes']['large']; ?>" alt="<?= $imageLeft['alt'] ?>">
      </div>
    </div>

  <?php else : ?>

    <div class="text-block">
      <div class="text-block__text">
        <h3><?= $text ?></h3>
      </div>
    </div>

  <?php endif; ?>

  </div>
</section>
