<?php
/**
 * The template part for displaying the text block
 */

// vars
$content = get_sub_field('content');


// Section settings
$settings = get_sub_field('section_settings');
$bg_colour = $settings['section_background_colour'];
$bg_image = $settings['section_background_image'];

if ($settings['section_text_colour'] !== 'default') $classes[] = $settings['section_text_colour'];

if($bg_colour) $classes[] = 'has-bg-color';
if($bg_image) $classes[] = 'has-bg-image';

if($bg_colour || $bg_image) {
  $style = $bg_image ? 'background-image: url('.$bg_image.');' : 'background-color: '.$bg_colour.';';
}

$classes = $classes ? implode(' ', $classes ) : '';

if( have_rows('testimonials') ):

?>
<section class="<?= $classes ?>" style="<?= $style ?>">
  <div class="container">
    <ul class="testimonials">
      <?php while ( have_rows('testimonials') ) : the_row(); ?>
        <li>
          <h2><?= get_sub_field('title'); ?></h2>
          <p><?= get_sub_field('text'); ?></p>
          <small><?= get_sub_field('label'); ?></small>
        </li>
      <?php endwhile; ?>
    </ul>
  </div>
</section>

<?php endif;
