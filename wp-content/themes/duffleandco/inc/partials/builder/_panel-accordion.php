<?php
/**
 * The template part for displaying the text block
 */


// Template Settings
$heading = get_sub_field('heading');

// Section settings
$settings = get_sub_field('section_settings');
$bg_colour = $settings['section_background_colour'];
$bg_image = $settings['section_background_image'];

if ($settings['section_text_colour'] !== 'default') $classes[] = $settings['section_text_colour'];

if($bg_colour) $classes[] = 'has-bg-color';
if($bg_image) $classes[] = 'has-bg-image';

if($bg_colour || $bg_image) {
  $style = $bg_image ? 'background-image: url('.$bg_image.');' : 'background-color: '.$bg_colour.';';
}

$classes[] = 'vertical';

$classes = $classes ? implode(' ', $classes ) : '';

?>

<section class="<?= $classes ?>" style="<?= $style ?>">
  <div class="container">

    <div class="grid-stretched">

      <div>
        <h1 class="vertical__heading"><?= $heading ?></h1>
      </div>

      <?php // Start panels
      if( have_rows('panels') ):
        $count = 0;
      ?>
      <div class="panels">

        <?php
        while ( have_rows('panels') ) : the_row();

          // vars
          $panel_classes = array();
          $panel_classes[] = 'panel';
          $id = 'panel' . $count;
          $buttons_markup = '';
          $gallery_markup = '';

          // fields
          $title = get_sub_field('title');
          $style = get_sub_field('style');
          $gallery = get_sub_field('gallery');
          $content_heading = get_sub_field('content_heading') ? '<h3>'.get_sub_field('content_heading').'</h3>' : '';
          $content_text = get_sub_field('content_text');
          $buttons = get_sub_field('buttons');
          $text_colour_class = get_sub_field('text_colour') !== 'default' ? get_sub_field('text_colour') : '';

          if ($style == 'Animated Icon') {
            $icon = get_sub_field('icon')['sizes']['medium'];
            $icon_hover = get_sub_field('icon_hover')['sizes']['medium'];
            $background_style = 'background-color:'.get_sub_field('background_colour').';';
            $content_style = $background_style;
            $anim_icon_markup = '<div class="panel__image"><div style="background-image: url('.$icon.')"></div><div style="background-image: url('.$icon_hover.')"></div></div>';
          } else {
            $background_style = 'background-image: url('.get_sub_field('background_image')['sizes']['medium'].');';
            $panel_classes[] = 'has-bg-img';
            $content_style = 'background-color: #161d33;';
            $text_colour_class = 'text-white';
          }

          if($gallery) {
            foreach( $gallery as $image ):
              $gallery_markup .= '<li><span style="'.$content_style.'"></span><div class="image-slider__image" style="background-image: url('.$image['sizes']['large'].');"></div></li>';
            endforeach;
            $gallery_markup = '<ul class="image-slider">'.$gallery_markup.'</ul>';
          }

          if( have_rows('buttons') ):
            while ( have_rows('buttons') ) : the_row();

              $hash = get_sub_field('button_link_to_section') ? '#section-' . get_sub_field('button_link_to_section') : '';
              $url = get_sub_field('button_url') . $hash;
              $target = get_sub_field('button_type') == 'Page' ? '_self' : '_blank';
              $buttons_markup .= '<a href="'.$url.'" target="'.$target.'" class="active">'.get_sub_field('button_text').'</a>';
            endwhile;
            $buttons_markup = '<div class="buttons">'.$buttons_markup.'</div>';
          endif;

          $panel_classes[] = $text_colour_class;
          $panel_classes = implode(' ', $panel_classes );

          $panel_titles .= '<div class="'.$panel_classes.'" style="'.$background_style.'" data-id="'.$id.'">
                              <span class="panel__toggle"></span>
                              <div class="panel__content">'.$anim_icon_markup.'
                                <span>'.$title.'</span>
                              </div>
                            </div>';

          $panel_content .= '<div id="'.$id.'" class="'.$text_colour_class.'" style="'.$content_style.'">'.$gallery_markup.'
                              <div class="entry-content">'.$content_heading.'
                              <p>'.$content_text.'</p>'.$buttons_markup.'
                              </div>
                            </div>';

          $count++;

          if ($count % 3 == 0) {
            echo '<div class="panel__titles">'.$panel_titles.'</div>';
            echo '<div class="panels__content">'.$panel_content.'</div>';
            $panel_titles = '';
            $panel_content = '';
          }

        endwhile;

        // print any leftovers

        if($panel_titles && $panel_content) {
          echo '<div class="panel__titles">'.$panel_titles.'</div>';
          echo '<div class="panels__content">'.$panel_content.'</div>';
        }

        ?>

      </div>

      <?php endif; // End panels ?>

      <div class="vertical__buttons"><div class="vb_wrap">
      <?php // start column 3 CTA's
      if( have_rows('ctas') ): ?>
      <?php
        while ( have_rows('ctas') ) : the_row();
        $cta = get_sub_field('cta');
        $hash = $cta['link_to_section'] ? '#section-' . $cta['link_to_section'] : '';
        $url = $cta['url'] . $hash;
        $target = $cta['type'] == 'Page' ? '_self' : '_blank';
        ?>
          <a href="<?= $url; ?>" class="arrow-btn arrow-btn--vertical" target="<?= $target; ?>"><span><?= $cta['text']; ?></span></a>
        <?php endwhile; ?>
      <?php endif; ?>
      </div></div>

    </div>

  </div>
</section>
