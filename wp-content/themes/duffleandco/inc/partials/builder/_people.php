<?php
$heading = get_sub_field('heading');
// $display_type = get_sub_field('display_type');

// Section settings
$settings = get_sub_field('section_settings');
$bg_colour = $settings['section_background_colour'];
$bg_image = $settings['section_background_image'];

if ($settings['section_text_colour'] !== 'default') $classes[] = $settings['section_text_colour'];

if($bg_colour) $classes[] = 'has-bg-color';
if($bg_image) $classes[] = 'has-bg-image';

if($bg_colour || $bg_image) {
  $style = $bg_image ? 'background-image: url('.$bg_image.');' : 'background-color: '.$bg_colour.';';
}

$classes[] = 'vertical';

$classes = $classes ? implode(' ', $classes ) : '';

?>
<section class="<?= $classes ?>" style="<?= $style ?>">
  <div class="container">

    <div class="grid-stretched">

      <div>
        <h1 class="vertical__heading"><?= $heading ?></h1>
      </div>

      <?php
      if( have_rows('people') ): ?>
        <ul class="people">
          <?php
            while ( have_rows('people') ) : the_row(); ?>
            <li class="person">
              <div class="person__image" style="background-image: url(<?= get_sub_field('image')['sizes']['medium']; ?>)"></div>
              <p class="person__name"><?= get_sub_field('name'); ?></p>
              <p class="person__title"><?= get_sub_field('title'); ?></p>
              <p class="person__description"><?= get_sub_field('description'); ?></p>
            </li>
          <?php endwhile; ?>
        </ul>
      <?php endif; ?>

      <div class="vertical__buttons">
        <div>
          <!-- <a href="#" class="arrow-btn arrow-btn--vertical">Our Products</a>
          <a href="#" class="arrow-btn arrow-btn--vertical">Lorem ipsum</a> -->
        </div>
      </div>

    </div>

  </div>
</section>
