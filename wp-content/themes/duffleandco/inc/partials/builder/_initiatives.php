<?php
$heading = get_sub_field('heading');

if( have_rows('initiatives') ): ?>
<section>
  <div class="container">
    <div class="tabs-wrap">

      <div class="grid">
      <?php
      $i = 0;

      while ( have_rows('initiatives') ) : the_row();

        // vars
        $name = get_sub_field('name');
        $content = get_sub_field('content');
        $product = get_sub_field('product') ? get_product( get_sub_field('product') ) : '';
        $active_class = $i == 0 ? 'tab-active' : '';

        // Tab headers markup
        $tabs_markup .= '<a href="#" data-id="tab'.$i.'" class="'.$active_class.'">'.$name.'</a>';

        // Content markup
        $contents_markup .= '<div data-rel="tab'.$i.'" class="'.$active_class.'">'.$content.'</div>';

        // Product Markup
        $features = array_merge(get_field('product_features', $product->id), get_field('product_packaging_stationary', $product->id));

        // Get product features to build icon list
        if( $features ) {

          foreach( $features as $feature ) {
            $features_markup .='<li>
                                  <i class="duffleicon duffleicon--'. strtolower(str_replace(' ', '-', $feature)) .'"></i>
                                  <div class="tooltip">'. $feature .'</div>
                                </li>';
          }

          $features_markup = '<ul class="product__features">'.$features_markup.'</ul>';

        } else $features_markup = '';

        // Add product markup to var
        if ($product) {
          $products_markup .= '<div data-rel="tab'.$i.'" class="'.$active_class.'">
                                <div class="ini-product">
                                  <a href="/product/'.$product->slug.'" class="ini-product__image" style="background-image: url('.wp_get_attachment_image_src($product->image_id, 'large')[0].');"></a>
                                  <div class="ini-product__description">'.$features_markup.'
                                    <h5>'. $product->name .'</h5>
                                    <div class="price">$'. $product->regular_price .'</div>
                                    <p>'. $product->short_description .'</p>
                                    <button data-quantity="1" data-product_id="'. $product->id .'" class="active ajax_add_to_cart add_to_cart_button">Add to cart</button>
                                  </div>
                                </div>
                              </div>';
        }

        $i++;

      endwhile; ?>

        <div class="grid__column grid__column--7">

          <h2><?= $heading; ?></h2>

          <div class="tabs">
            <?= $tabs_markup; ?>
          </div>

          <div class="tabs-content entry-content">
            <?= $contents_markup; ?>
          </div>

        </div>

        <div class="grid__column grid__column--5">
          <div class="tabs-content">
            <?= $products_markup; ?>
          </div>
        </div>


      </div>

    </div>
  </div>
</section>
<?php endif; ?>
