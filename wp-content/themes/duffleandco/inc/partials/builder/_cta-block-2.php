<?php
/**
 * The template part for displaying the text block
 */

// vars

$heading = get_sub_field('heading');
$text = get_sub_field('text');
$buttons = get_sub_field('buttons');

// Section settings
$settings = get_sub_field('section_settings');
$bg_colour = $settings['section_background_colour'];
$bg_image = $settings['section_background_image'];

if ($settings['section_text_colour'] !== 'default') $classes[] = $settings['section_text_colour'];

if($bg_colour) $classes[] = 'has-bg-color';
if($bg_image) $classes[] = 'has-bg-image';

if($bg_colour || $bg_image) {
  $style = $bg_image ? 'background-image: url('.$bg_image.');' : 'background-color: '.$bg_colour.';';
}

$classes[] = 'cta-block';
$classes[] = 'vertical';

$classes = $classes ? implode(' ', $classes ) : '';

?>
<section class="<?= $classes ?>" style="<?= $style ?>">
  <div class="container">
    <div class="grid">
      <div class="grid__column grid__column--7">
        <h2><?= $heading ?></h2>
        <p><?= $text ?></p>
      </div>

      <?php
      if( have_rows('buttons') ): ?>
      <div class="grid__column">
        <div class="vertical__buttons">
          <div class="vb_wrap">
          <?php
            while ( have_rows('buttons') ) : the_row();
              $hash = get_sub_field('button_link_to_section') ? '#section-' . get_sub_field('button_link_to_section') : '';
              $url = get_sub_field('button_url') . $hash;
              $target = get_sub_field('button_type') == 'Page' ? '_self' : '_blank';
              ?>
            <a href="<?= $url; ?>" class="arrow-btn arrow-btn--vertical" target="<?= $target; ?>"><span><?= get_sub_field('button_text'); ?></span></a>
          <?php endwhile; ?>
          </div>
        </div>
      </div>
      <?php endif; ?>

    </div>
  </div>
</section>

