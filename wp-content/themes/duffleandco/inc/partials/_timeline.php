<section>
  <div class="container">
    <div class="timeline">
      <div class="timeline__list">
        <i class="duffleicon duffleicon--goat timeline__icon"></i>
        <div class="timeline__bar"></div>
        <ul>
          <li class="timeline__row">
            <div class="timeline__text">
              <h5>The Beginning</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
            </div>
            <div class="timeline__image">
              <img src="/ui/images/timeline-1.jpg" alt="" />
            </div>
          </li>
          <li class="timeline__row">
            <div class="timeline__text">
              <h5>The Beginning</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
            </div>
            <div class="timeline__image">
              <img src="/ui/images/timeline-1.jpg" alt="" />
            </div>
          </li>
          <li class="timeline__row">
            <div class="timeline__text">
              <h5>The Beginning</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
            </div>
            <div class="timeline__image">
              <img src="/ui/images/timeline-1.jpg" alt="" />
            </div>
          </li>
          <li class="timeline__row">
            <div class="timeline__text">
              <h5>The Beginning</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
            </div>
            <div class="timeline__image">
              <img src="/ui/images/timeline-1.jpg" alt="" />
            </div>
          </li>
        </ul>
      </div>
      <div class="timeline__row">
        <div class="timeline__text">
          <h5>The End</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
        </div>
        <div class="timeline__image">
          <img src="/ui/images/timeline-1.jpg" alt="" />
        </div>
      </div>
    </div>
  </div>
</section>
