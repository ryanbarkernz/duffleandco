<section class="has-bg-color" style="background-color: #f7f7f7;">
  <div class="container">

    <div class="social__header">
      <h2>Social</h2>

      <div class="vertical__buttons">
        <div class="vb_wrap">
          <a href="#" class="arrow-btn arrow-btn--vertical"><span>The Shop</span></a>
        </div>
      </div>
    </div>

  </div>

  <ul class="insta"></ul>

  <div class="container">
    <div class="social__footer">
      <a href="https://www.instagram.com/duffleandco/" target="_blank" class="instagram"><i class="icon-social-instagram icons"></i> @duffleandco</a>
      <a href="https://www.facebook.com/duffleandco" target="_blank" class="facebook"><i class="icon-social-facebook icons"></i> /duffleandco</a>
    </div>
  </div>

</section>
