<section>
  <div class="container">
    <div class="tabs-wrap">

      <div class="grid">
        <div class="grid__column grid__column--7">

          <h2>Our Initiatives</h2>

          <div class="tabs">
            <a href="#" data-id="tab1" class="is-active">Initiative Name1</a>
            <a href="#" data-id="tab2">Initiative Name2</a>
          </div>

          <div class="tabs-content entry-content">
            <div data-rel="tab1" class="is-visible">
              <p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Find out more. exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              <p>Enderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ecepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
              <br />
              <img src="http://placehold.it/300x100" />
            </div>
            <div data-rel="tab2">
              <p>Porem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Find out more. exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              <p>Enderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ecepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
            </div>
          </div>

        </div>

        <div class="grid__column grid__column--5">
          <div class="tabs-content">
            <div data-rel="tab1" class="is-visible">

              <?php $product = get_product( 18 ); //var_dump($product); ?>
              <div class="ini-product">
                <a href="/product/<?= $product->slug ?>" class="ini-product__image" style="background-image: url(<?php echo wp_get_attachment_image_src($product->image_id, 'large')[0]; ?>);">
                </a>
                <div class="ini-product__description">
                  <?php
                  // FEATURE ICONS
                  $features = array_merge(get_field('product_features', $product->id), get_field('product_packaging_stationary', $product->id));
                  if( $features ): ?>
                  <ul class="product__features">
                    <?php foreach( $features as $feature ): ?>
                      <li>
                        <i class="duffleicon duffleicon--<?= strtolower(str_replace(' ', '-', $feature)); ?>"></i>
                        <div class="tooltip"><?= $feature ?></div>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                  <?php endif; ?>

                  <h5><?= $product->name ?></h5>
                  <div class="price">$<?= $product->price ?></div>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  <button data-quantity="1" data-product_id="<?= $product->id; ?>" class="active ajax_add_to_cart add_to_cart_button">Add to cart</button>

                </div>
              </div>

            </div>
            <div data-rel="tab2">Product 2</div>
          </div>
        </div>

      </div>

    </div>
  </div>
</section>
