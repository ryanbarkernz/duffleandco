
<div class="icon-guide">
  <a href="#" class="close-icon-guide"> Close</a>
  <h2>Icon guide</h2>
  <ul>
    <?php if (get_field('vegan', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--vegan"></i>
        <h3>Vegan Friendly</h3>
        <p><?= get_field('vegan', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('organic_bamboo', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--organic-bamboo"></i>
        <h3>Organic Bamboo</h3>
        <p><?= get_field('organic_bamboo', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('hand_crafted', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--hand-crafted"></i>
        <h3>Hand Crafted</h3>
        <p><?= get_field('hand_crafted', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('organic_cotton', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--organic-cotton"></i>
        <h3>Organic Cotton</h3>
        <p><?= get_field('organic_cotton', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('planting_trees', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--planting-trees"></i>
        <h3>Planting Trees</h3>
        <p><?= get_field('planting_trees', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('uv_polarised', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--uv-polarised"></i>
        <h3>UV Polarised</h3>
        <p><?= get_field('uv_polarised', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('feeding_kiwis', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--kiwi"></i>
        <h3>Feeding Kiwis</h3>
        <p><?= get_field('feeding_kiwis', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('earth_friendly', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--earth-friendly"></i>
        <h3>Earth Friendly</h3>
        <p><?= get_field('earth_friendly', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('supporting_communities', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--supporting-communities"></i>
        <h3>Supporting Communities</h3>
        <p><?= get_field('supporting_communities', 'option'); ?></p>
      </li>
    <?php endif; ?>

    <?php if (get_field('ocean_friendly', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--ocean-friendly"></i>
        <h3>Ocean Friendly</h3>
        <p><?= get_field('ocean_friendly', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('recycled_paper', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--recycled-paper"></i>
        <h3>Recycled Paper</h3>
        <p><?= get_field('recycled_paper', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('biodegradable', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--biodegradable"></i>
        <h3>Biodegradable</h3>
        <p><?= get_field('biodegradable', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('plastic_free', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--plastic-free"></i>
        <h3>Plastic Free</h3>
        <p><?= get_field('plastic_free', 'option'); ?></p>
      </li>
    <?php endif; ?>
    <?php if (get_field('recycled_materials', 'option')) : ?>
      <li>
        <i class="duffleicon duffleicon--recycled-materials"></i>
        <h3>Recycled Materials</h3>
        <p><?= get_field('recycled_materials', 'option'); ?></p>
      </li>
    <?php endif; ?>
  </ul>
</div>
