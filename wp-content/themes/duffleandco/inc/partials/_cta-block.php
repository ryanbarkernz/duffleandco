<section class="cta-block has-bg text-white vertical" style="background-image: url('/ui/images/team.jpg');">
  <div class="container">
    <div class="grid">
      <div class="grid__column grid__column--7">
        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
      </div>
      <div class="grid__column grid__column--5">
        <div class="vertical__buttons">
          <div class="vb_wrap">
            <a href="#" class="arrow-btn arrow-btn--vertical">Our Products</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
