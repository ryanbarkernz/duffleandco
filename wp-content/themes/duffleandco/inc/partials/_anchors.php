<div class="anchors">
  <div class="container">
    <ul>
      <li><a href="#vision">Vision</a></li>
      <li><a href="#whoweare">Who we are</a></li>
      <li><a href="#initiatives">Initiatives</a></li>
      <li><a href="#story">Story</a></li>
      <li><a href="#faqs">FAQs</a></li>
    </ul>
  </div>
</div>
