<?php
  $id = get_the_ID();

  // Get the posts
  $related_posts = get_posts( apply_filters('woocommerce_product_related_posts', array(
      'orderby' => 'rand',
      'posts_per_page' => 3,
      'post__not_in' => array($id),
      'post_type' => 'product',
      'fields' => 'ids'
  ) ) );

  if ($related_posts) :

?>
<section class="featured-products">
  <div class="container">

    <div class="fp__header">
      <h2>You might also like...</h2>
      <div class="categories">
      </div>
      <div class="vertical__buttons">
        <div class="vb_wrap">
          <a href="/shop" class="arrow-btn arrow-btn--vertical">The Shop</a>
        </div>
      </div>
    </div>

    <ul class="fp__products">

      <?php foreach( $related_posts as $related_post_id ):

        $product = get_product( $related_post_id );
        $initiative_icon = get_field('product_initiative_icon', $product->id);
        $initiative_text = get_field('product_initiative_text', $product->id);
        // var_dump($product->image_id);
        ?>

        <li class="fp__product" data-cat="<?= join(', ', $product->category_ids) ?>" data-visibility="visible">
          <a href="/product/<?= $product->slug ?>" class="product__image" style="background-image: url(<?php echo wp_get_attachment_image_src($product->image_id, 'large')[0]; ?>);">
            <div class="product__hover">
              <?php if ($initiative_icon) : ?><i class="duffleicon <?= $initiative_icon; ?>"></i><?php endif; ?>
              <?php if ($initiative_text) : ?><p><?= $initiative_text; ?></p><?php endif; ?>
              <div class="text-btn text-btn--sml text-btn--active">View product</div>
            </div>
          </a>
          <div class="product__description">
            <?php
            // FEATURE ICONS
            $product_features = get_field('product_features', $product->id) ? get_field('product_features', $product->id) : [];
            $product_packaging_stationary = get_field('product_packaging_stationary', $product->id) ? get_field('product_packaging_stationary', $product->id) : [];
            $features = array_merge($product_features, $product_packaging_stationary);
            if( $features ): ?>
            <ul class="product__features">
              <?php foreach( $features as $feature ): ?>
                <li>
                  <i class="duffleicon duffleicon--<?= strtolower(str_replace(' ', '-', $feature)); ?>"></i>
                  <div class="tooltip"><?= $feature ?></div>
                </li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>

            <h4><?= $product->name ?></h4>
            <p class="attributes"><?= str_replace(',', ' |', $product->get_attribute( 'pa_colour' )) ?></p>
            <div class="price">$<?= $product->price ?></div>

          </div>
        </li>

      <?php endforeach; ?>

      <li class="fp__link" style="background-color: #fff;">
        <a href="/shop" class="active">All products</a>
      </li>

    </ul>

  </div>
</section>

<?php endif; ?>
